<?php
/**
 * @file
 * Provides a View area handler to be used as footer for quotations.
 */

/**
 * Area handler to display the empty text message for quotations.
 */
class commerce_quotation_handler_area_quotation_footer extends views_handler_area {

  function render($empty = FALSE) {
    return theme('commerce_quotation_footer');
  }

}
