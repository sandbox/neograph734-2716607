<?php
/**
 * @file
 * Provides a View area handler to be used as empty result for quotations.
 */

/**
 * Area handler to display the empty text message for quotations.
 */
class commerce_quotation_handler_area_quotation_empty_text extends views_handler_area {

  function render($empty = FALSE) {
    // Empty quotation Views pages (and any variant of) will output the
    // theme for empty quotation pages.
    if ($this->view->display_handler instanceof views_plugin_display_page) {
      $theme_hook = 'commerce_quotation_empty_page';
    }

    // All other display handlers (that includes blocks, attachments, content
    // panes, etc.) will fallback to using the block variant of the empty
    // quotation theme.
    else {
      $theme_hook = 'commerce_quotation_empty_block';
    }

    return theme($theme_hook);
  }

}
