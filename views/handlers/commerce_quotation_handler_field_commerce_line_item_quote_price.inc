<?php
/**
 * @file
 *
 * A Views' field handler for editing a node title.
 *
 */

/**
 * Field handler to display a text field to edit the unit price.
 */
class commerce_quotation_handler_field_commerce_line_item_quote_price extends views_handler_field {

  function render($values) {
    return '<!--form-item-' . $this->options['id'] . '--' . $this->view->row_index . '-->';
  }

  /**
   * Add to and alter the form created by Views.
   */
  function views_form(&$form, &$form_state) {
    // The view is empty, abort.
    if (empty($this->view->result)) {
      return;
    }

    $form[$this->options['id']] = array(
      '#tree' => TRUE,
    );

    // Iterate over the result and add our replacement fields to the form.
    foreach ($this->view->result as $row_index => $row) {
      if ($line_item = $this->get_value($row)) {

        $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
        $amount = $line_item_wrapper->commerce_unit_price->amount->value();
        $currency_code = commerce_default_currency();

        // First load the currency array.
        $currency = commerce_currency_load($currency_code);

        // Convert the price amount to the currency's major unit decimal value.
        $amount = commerce_currency_amount_to_decimal($amount, $currency_code);

        // Format the price as a number.
        $price = number_format(commerce_currency_round(abs($amount), $currency), $currency['decimals'], $currency['decimal_separator'], $currency['thousands_separator']);

        // Add a text field to the form.  This array convention
        // corresponds to the placeholder HTML comment syntax.
        $form[$this->options['id']][$row_index] = array(
          '#type' => 'textfield',
          '#default_value' => $price,
          '#field_prefix' => '€',
          '#required' => FALSE,
          '#size' => 10,
        );
      }
    }

    // Run this as the last callback so we can properly update the order totals.
    $form['#submit'][] = 'commerce_quotation_recalculate_order_totals';
  }

  /**
   * Form submit method.
   */
  function views_form_submit($form, &$form_state) {
    // List of orders to update in the end.
    $order_numbers = array();

    // Iterate over the view result.
    foreach ($this->view->result as $row_index => $row) {
      // Grab the correspondingly submitted form value.
      $value = $form_state['values'][$this->options['id']][$row_index];

      if ($line_item = $this->get_value($row)) {
        $unit_price = str_replace(',', '.', $value);
        $unit_price = commerce_currency_decimal_to_amount($unit_price, commerce_default_currency());

        $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
        $line_item_wrapper->commerce_unit_price->amount->set($unit_price);
        commerce_line_item_rebase_unit_price($line_item_wrapper->value());
        commerce_line_item_save($line_item_wrapper->value());

        // Put the order up for updating, prevent dupes.
        $order_numbers[$line_item_wrapper->order_id->value()] = $line_item_wrapper->order_id->value();
      }
    }

    $form_state['#asalight_orders'] = $order_numbers;
  }

}

// Run this as the last hook so we can properly update the order totals.
function commerce_quotation_recalculate_order_totals($form, &$form_state) {
  foreach ($form_state['#asalight_orders'] as $order_id) {
    $order = commerce_order_load($order_id);
    commerce_order_calculate_total($order);
    commerce_order_save($order);
  }
}
