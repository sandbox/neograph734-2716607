<?php
/**
 * @file
 * Contains the Views handlers for commerce quotes.
 */

/**
 * Implements hook_views_data_alter().
 */
function commerce_quotation_views_data_alter(&$data) {
  // Add a price field to commerce line items.
  $data['commerce_line_item']['quote_price'] = array(
    'real field' => 'line_item_id',
    'field' => array(
      'title' => t('Price text field'),
      'help' => t('Adds a text field to edit the line item price in the View.'),
      'handler' => 'commerce_quotation_handler_field_commerce_line_item_quote_price',
    ),
  );

  $data['views_entity_commerce_order']['quotation_empty_text'] = array(
    'title' => t('Empty quotation'),
    'help' => t('Displays an appropriate empty text message for quotations.'),
    'area' => array(
      'handler' => 'commerce_quotation_handler_area_quotation_empty_text',
    ),
  );

  $data['views_entity_commerce_order']['quotation_header'] = array(
    'title' => t('Quotation header'),
    'help' => t('Displays a header for quotations.'),
    'area' => array(
      'handler' => 'commerce_quotation_handler_area_quotation_header',
    ),
  );

  $data['views_entity_commerce_order']['quotation_footer'] = array(
    'title' => t('Quotation footer'),
    'help' => t('Displays a footer for quotations.'),
    'area' => array(
      'handler' => 'commerce_quotation_handler_area_quotation_footer',
    ),
  );
}
