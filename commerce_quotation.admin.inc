<?php
/**
 * @file
 * Administrative pages of the module.
 */

/**
 * Main configuration form.
 */
function commerce_quotation_admin_form() {

  // Limits products available for quoting.
  $form['commerce_quotation_product_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Product types'),
    '#description' => t('Select on which product types should the "Add to quote" button be displayed. If left empty, the button will be displayed on all product types.'),
    '#options' => commerce_product_type_options_list(),
    '#default_value' => variable_get('commerce_quotation_product_types', array()),
  );

  return system_settings_form($form);
}
