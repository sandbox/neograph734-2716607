<?php
/**
 * @file
 * Provides metadata for the quotation orders.
 */

/**
 * Implements hook_entity_property_info_alter().
 */
function commerce_quotation_entity_property_info_alter(&$info) {

  $properties = &$info['commerce_order']['properties'];

  // Add the current user's shopping cart to the site information.
  $properties['commerce_quotation_url'] = array(
    'label' => t("Quotation URL"),
    'description' => t("The URL of the quotation."),
    'getter callback' => 'commerce_quotation_get_properties',
    'type' => 'uri',
    'computed' => TRUE,
    // Make the field available as views field.
    'entity views field' => TRUE,
  );

  $properties['commerce_quotation_edit_url'] = array(
    'label' => t("Quotation edit URL"),
    'description' => t("The URL of the quotation edit page."),
    'getter callback' => 'commerce_quotation_get_properties',
    'type' => 'uri',
    'computed' => TRUE,
    // Make the field available as views field.
    'entity views field' => TRUE,
  );
}
