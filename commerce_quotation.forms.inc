<?php
/**
 * @file
 * Contains forms and submission functions for all forms.
 */

/**
 * Main quotation form.
 */
function commerce_quotation_form($form, &$form_state, $commerce_order = NULL) {
  global $user;
  $uid = $user->uid;

  if (empty($commerce_order)) {
    // Prepare empty order if none given.
    $commerce_order = commerce_order_new($uid, 'quote');
  }

  // Store the entity for later validation and submission.
  $form_state['#commerce_quotation_order'] = $commerce_order;

  // Fetch a list of all field instances on commerce orders.
  $fields = _field_invoke_get_instances('commerce_order', 'commerce_order', array('deleted' => FALSE));

  // Attach all field instances with 'commerce_quote_show' enabled.
  foreach ($fields as $field => $details) {
    if ($details['settings']['commerce_quotation_show']) {
      field_attach_form('commerce_order', $commerce_order, $form, $form_state, NULL, array('field_name' => $field));
    }
  }

  // Add the form actions.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 5,
  );

  if (!empty($commerce_order->order_id)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#weight' => 15,
      '#submit' => array('commerce_quotation_form_deletebutton_submit'),
    );
  }

  return $form;
}

/**
 * Validation for the commerce_quote form.
 */
function commerce_quotation_form_validate($form, &$form_state) {
  field_attach_form_validate('commerce_order', $form_state['#commerce_quotation_order'], $form, $form_state);
}

/**
 * Submission of the commerce_quote form.
 */
function commerce_quotation_form_submit($form, &$form_state) {
  $commerce_order = $form_state['#commerce_quotation_order'];

  if (commerce_order_save($commerce_order)) {
    field_attach_submit('commerce_order', $commerce_order, $form, $form_state);
    $form_state['redirect'] = $commerce_order->commerce_quotation_url;

    // Store this in the session, so it is selected by default.
    $_SESSION['commerce_quote']['latest'] = $commerce_order->order_id;

    drupal_set_message(t('Quote saved successfully.'));
  }
}

/**
 * Handles the delete button on the commerce_quotation form.
 */
function commerce_quotation_form_deletebutton_submit($form, &$form_state) {
  $destination = array();
  if (isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }
  $commerce_order = $form_state['build_info']['args'][0];
  $form_state['redirect'] = array($commerce_order->commerce_quotation_url . '/delete', array('query' => $destination));
}

/**
 * Delete confirmation form for quotations.
 */
function commerce_quotation_confirm_delete_form($form, &$form_state, $commerce_order) {
  $form_state['#commerce_order'] = $commerce_order;

  $message = t('Are you sure you want to remove quote %title?', array(
    '%title' => commerce_quotation_get_quote_title($commerce_order)
  ));

  return confirm_form(
    $form,
    $message,
    $commerce_order->commerce_quotation_url,
    NULL,
    t('Delete')
  );
}

/**
 * Submit handler for quotation deletion.
 *
 * @see commerce_quote_quote_delete_form()
 */
function commerce_quotation_confirm_delete_form_submit($form, &$form_state) {
  $entity = $form_state['#commerce_order'];
  list($id) = entity_extract_ids('commerce_order', $entity);

  // Delete the commerce_order.
  if (commerce_order_delete($id)) {
    $form_state['redirect'] = 'user/' . $entity->uid . '/quotes';
    drupal_set_message(t('Quote has been deleted successfully.'));
  }
}

/**
 * Quote approval form.
 *
 * @param $form
 * @param $form_state
 * @param $user
 * @param $order
 *
 * @return mixed
 *   Description of the parameter
 */
function commerce_quotation_confirm_approval_form($form, &$form_state, $user, $commerce_order) {
  // Store these for later.
  $form_state['#order'] = $commerce_order;

  return confirm_form(
    $form,
    t('Are you sure you want to confirm the quote?'),
    $commerce_order->commerce_quotation_url,
    t('By confirming this quote, you agree to purchase the items on it.')
  );
}

/**
 * Submit function for the quote approval form.
 *
 * @see commerce_quotation_approve_confirmation_form()
 */
function commerce_quotation_confirm_approval_form_submit($form, &$form_state) {
  rules_invoke_event('commerce_quotation_approved', $form_state['#order']);
  $form_state['redirect'] = $form_state['#order']->commerce_quotation_url;
}

/**
 * @param $form
 * @param $form_state
 * @param $user
 * @param $commerce_order
 *
 * @return mixed
 *   Description of the parameter
 */
function commerce_quotation_send_form($form, &$form_state, $user, $commerce_order) {
  $form_state['#order'] = $commerce_order;

  $default_message = "Dear user,

Thanks for contacting us. Attached you will find your quotation [link]";

  $form['message'] = array(
    '#type' => 'textarea',
    '#title' => t('Message'),
    '#description' => t('The message to send along with the quotation.'),
    '#default_value' => $default_message,
  );

  if (module_exists('urllogin')) {
    $form['urllogin'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use URL Login'),
      '#description' => t('This can be used to make clients log in automatically from the link in the mail.'),
    );
  }

  // Add the form actions.
  $form['actions'] = array('#type' => 'actions', '#weight' => 10);
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
    '#weight' => 5,
  );

  return $form;
}

/**
 * @param $form
 * @param $form_state
 *   Description of the parameter
 */
function commerce_quotation_send_form_submit($form, &$form_state) {
  rules_invoke_event('commerce_quotation_prepared', $form_state['#order'], $form_state['values']['message']);
  $form_state['redirect'] = $form_state['#order']->commerce_quotation_url;
}

/**
 * @param $form
 * @param $form_state
 * @param $user
 * @param $commerce_order
 *
 * @return mixed
 *   Description of the parameter
 */
function commerce_quotation_request_form($form, &$form_state, $user, $commerce_order) {
  // Store these for later.
  $form_state['#user'] = $user;
  $form_state['#order'] = $commerce_order;

  return confirm_form(
    $form,
    t('Are you sure you want to send this quotation request to the shopkeeper?'),
    $commerce_order->commerce_quotation_url,
    t('After sending the request you will no longer be able to add products to this request until the shopkeeper has responded to you with a quotation.')
  );
}

/**
 * @param $form
 * @param $form_state
 * @param $commerce_order
 *   Description of the parameter
 */
function commerce_quotation_request_form_submit($form, &$form_state, $commerce_order) {
  rules_invoke_event('commerce_quotation_requested', $form_state['#order']);
  $form_state['redirect'] = $form_state['#order']->commerce_quotation_url;
}
