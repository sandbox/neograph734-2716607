<?php
/**
 * @file
 * Contains all commerce hook implementations.
 */

/**
 * Implements hook_commerce_customer_profile_type_info().
 */
function commerce_quotation_commerce_customer_profile_type_info() {
  $profile_types = array();

  $profile_types['quotation'] = array(
    'type' => 'quotation',
    'name' => t('Quotation information'),
    'description' => t('The profile used to collect quotation information on the quotation forms.'),
    'help' => '',
    'addressfield' => TRUE,
  );

  return $profile_types;
}

/**
 * Implements hook_commerce_order_state_info().
 */
function commerce_quotation_commerce_order_state_info() {
  $order_states = array();

  $order_states['quote'] = array(
    'name' => 'quote',
    'title' => t('Quote'),
    'description' => t('Orders in this state are new, sent, returned or confirmed quotes.'),
    'weight' => -5,
    'default_status' => 'quote',
  );

  return $order_states;
}

/**
 * Implements hook_commerce_order_status_info().
 */
function commerce_quotation_commerce_order_status_info() {
  $order_statuses = array();

  // New quotations.
  $order_statuses['quote'] = array(
    'name'  => 'quote',
    'title' => t('Quote: Creation'),
    'state' => 'quote',
    'cart'  => FALSE,
  );

  // Quotations awaiting approval by clients.
  $order_statuses['quote_awaiting_approval'] = array(
    'name'  => 'quote_awaiting_approval',
    'title' => t('Quote: Awaiting approval'),
    'state' => 'quote',
    'cart'  => FALSE,
  );

  // Quotations confirmed by clients.
  $order_statuses['quote_confirmed'] = array(
    'name'  => 'quote_confirmed',
    'title' => t('Quote: Confirmed'),
    'state' => 'quote',
    'cart'  => FALSE,
  );

  // Quotations returned from clients.
  $order_statuses['quote_declined'] = array(
    'name'  => 'quote_declined',
    'title' => t('Quote: Declined'),
    'state' => 'quote',
    'cart'  => FALSE,
  );

  return $order_statuses;
}

/**
 * Implements hook_commerce_entity_access_condition_ENTITY_TYPE_alter() for commerce orders.
 *
 * Grants view permissions on received quotes.
 */
function commerce_quotation_commerce_entity_access_condition_commerce_order_alter(&$conditions, $context) {
  global $user;
  $uid = $user->uid;

  // Owners should already have access via Commerce access checks. Now we make
  // sure the initial creator (shopkeeper) also has access to quotes.
  $and = db_and();
  $and->condition($context['base_table'] . '.state', 'quote');
  $and->condition($context['base_table'] . '.uid', $uid);

  $or = db_or();
  $or->condition($and);

  $conditions->condition($or);
}

/**
 * Implements hook_commerce_entity_access_condition_ENTITY_TYPE_alter() for line items.
 *
 * Grants view permissions on line items of received quotes.
 */
function commerce_quotation_commerce_entity_access_condition_line_item_alter(&$conditions, $context) {

  // Match the line item order id.
  $and = db_and();
  $and->condition($context['base_table'] . '.order_id', 'quote');

  $or = db_or();
  $or->condition($and);

  $conditions->condition($or);
}
