<?php
/**
 * @file
 * Rules integration for Commerce Quotation.
 */

/**
 * Implements hook_rules_event_info().
 */
function commerce_quotation_rules_event_info() {
  return array(
    'commerce_quotation_created' => array(
      'label' => t('After creating a quote'),
      'help' => t('Triggers when a user has created a quote.'),
      'group' => t('Commerce Quotation'),
      'variables' => array(
        'commerce_order' => array(
          'type' => 'commerce_order',
          'label' => t('The commerce order that was created.'),
        ),
      ),
    ),
    'commerce_quotation_product_added' => array(
      'label' => t('After adding a product to a quote'),
      'help' => t('Triggers when a product has been added a quote.'),
      'group' => t('Commerce Quotation'),
      'variables' => array(
        'commerce_order' => array(
          'type' => 'commerce_order',
          'label' => t('The commerce order to which the product was added.'),
        ),
        'commerce_product' => array(
          'type' => 'commerce_product',
          'label' => t('The commerce product that has been added.'),
        ),
        'commerce_line_item' => array(
          'type' => 'commerce_line_item',
          'label' => t('The commerce line-item that has been added.'),
        ),
      ),
    ),
    'commerce_quotation_prepared' => array(
      'label' => t('After preparing a quote for a client'),
      'help' => t('Triggers when a user has prepared quote to a client.'),
      'group' => t('Commerce Quotation'),
      'variables' => array(
        'commerce_order' => array(
          'type' => 'commerce_order',
          'label' => t('The commerce order that was sent.'),
        ),
        'message' => array(
          'type' => 'text',
          'label' => t('The message.'),
        ),
      ),
    ),
    'commerce_quotation_requested' => array(
      'label' => t('After requesting a quote'),
      'help' => t('Triggers when a client has requested a quote.'),
      'group' => t('Commerce Quotation'),
      'variables' => array(
        'commerce_order' => array(
          'type' => 'commerce_order',
          'label' => t('The commerce quote that was requested.'),
        ),
      ),
    ),
    'commerce_quotation_approved' => array(
      'label' => t('After confirming a quote'),
      'help' => t('Triggers when a user has confirmed a quote.'),
      'group' => t('Commerce Quotation'),
      'variables' => array(
        'commerce_order' => array(
          'type' => 'commerce_order',
          'label' => t('The commerce order that was accepted.'),
        ),
      ),
    ),
    'commerce_quotation_declined' => array(
      'label' => t('After declining a quote'),
      'help' => t('Triggers when a user has declined a quote.'),
      'group' => t('Commerce Quotation'),
      'variables' => array(
        'commerce_order' => array(
          'type' => 'commerce_order',
          'label' => t('The commerce order that was declined.'),
        ),
      ),
    ),
  );
}
